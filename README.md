# hands-on-terraform

## What is Terraform ?

Terraform is an Infrastructure as Code (IaC) tool. It can also be described as a structured API management tool. It supports multiple cloud platforms, allows the use of a versioning system for better collaboration around infrastructure and tracks infrastructure changes throughout deployments with Terraform's states.

## Installation

Download Terraform binary here : https://www.terraform.io/downloads.html

Move it somewhere in your $PATH:
`mv ~/Downloads/terraform /usr/local/bin/`

Verify:
`terraform -help`

## CLI overview

Main commands:
  - `terraform init`          Prepare your working directory for other commands
  - `terraform validate'      Check whether the configuration is valid
  - `terraform plan`          Show changes required by the current configuration
  - `terraform apply`         Create or update infrastructure
  - `terraform destroy`       Destroy previously-created infrastructure

Other important commands:
  - `terraform fmt`           Reformat your configuration in the standard style
  - `terraform show`          Show the current state or a saved plan
  - `terraform import`        Associate existing infrastructure with a Terraform resource

## Playground

In this example, we will create (and destroy) a bunch of lambdas all together. Yay!

### Files overview

`cd examples/lambda`

`ls -l`

Each file plays a different role here:
  - `provider.tf` : the `terraform {}` block contains Terraform settings, the `provider {}` block contains the provider settings (aws in our case)
  - `main.tf`: the `resource {}` block contains the resource we want to create
  - `data.tf`: the `data {}` block contains already existing resources that we need to refer to
  - `variables.tf`: the `variable {}` block contains ... well ... variables definition
  - `terraform.tfvars`: file whose name is imposed and assign values to variables

### Running Terraform commands

#### Initialisation 

`terraform init`

#### Formating

`terraform fmt`

#### Validation

`terraform validate`

#### Planification

`terraform plan`

also

```
terraform plan -out out.tfplan
terraform show -json out.tfplan > tfplan.json
```

#### Deployment 

`terraform apply`

#### Destruction

`terraform destroy`

### Inspecting the state file

After `terraform apply`, Terraform wrote data into a `terraform.tfstate` file:

`terraform show`

### Storing the state file into S3

Open `provider.tf` and modify the `terraform {}` block:

```
terraform {
  required_version = ">= 0.14.9"
  backend "s3" {
    bucket = "bryter-eu-qa-information-extraction"
    key    = "terraform/backend/<MY_NAME>/terraform.tfstate"
    region = "eu-central-1"
  }
}
```

You might need to `export AWS_PROFILE=operations`
