resource "aws_lambda_function" "lambda" {
  function_name = var.function_name

  role = data.aws_iam_role.lambda_exec_role.arn

  package_type = "Image"
  image_uri    = "727543438143.dkr.ecr.eu-central-1.amazonaws.com/data-science/lambda-images/preprocessing:0.0.1-30892afd-test"

  memory_size = var.memory_size
  timeout     = var.timeout

  environment {
    variables = var.environment_variables
  }
}
