terraform {
  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "operations"
  region  = "eu-central-1"
}
