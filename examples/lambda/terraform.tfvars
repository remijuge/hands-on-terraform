function_name = "preprocessing-<MY_NAME>"
lambda_exec_role_name = "text-extraction-lambda"
memory_size = 128
timeout = 3
environment_variables = {
    ENVIRONMENT = "operations"
    LOG_LEVEL = "DEBUG"
}
