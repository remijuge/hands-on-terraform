variable "function_name" {
  type = string
}

variable "lambda_exec_role_name" {
  type = string
}

variable "memory_size" {
  type = number
}

variable "timeout" {
  type = number
}

variable "environment_variables" {
  type = map(string)
}
